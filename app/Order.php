<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'status',
        'orderDate',
        'estimatedDate',
        'total',
        'customer_id'
    ];

    function customer(){
        return $this->belongsTo('App\Customer');
    }
    function details(){
        return $this->hasMany('App\OrderDetail');
    }
}
