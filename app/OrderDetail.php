<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderDetail extends Model
{
    //

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function scopeBatches($query){
        return $query->select('code', 'design_id', DB::raw('SUM(quantity) as total'), DB::raw('MIN(estimatedDate) as date'))
            ->join('orders', 'orders.id', 'order_details.order_id')
            ->join('products', 'products.id', 'order_details.product_id')
            ->join('designs', 'designs.id', 'products.design_id')
            ->groupBy('design_id', 'code')
            ->where('orders.status', 'ordered');
    }

    public function scopeBatchDetails($query){
        return $query->select('products.id  as product_id', 'size', 'code', 'design_id', DB::raw('SUM(quantity) as total'), DB::raw('MIN(estimatedDate) as date, stock'))
            ->join('orders', 'orders.id', 'order_details.order_id')
            ->join('products', 'products.id', 'order_details.product_id')
            ->join('designs', 'designs.id', 'products.design_id')
            ->groupBy('design_id', 'products.id', 'size', 'stock', 'code')
            ->where('orders.status', 'ordered');
    }
}
