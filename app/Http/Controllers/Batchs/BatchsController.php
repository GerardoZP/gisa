<?php

namespace App\Http\Controllers\Batchs;

use App\Batch;
use App\BatchDetail;
use App\Design;
use App\DesignMaterial;
use App\Material;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\PurchaseDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BatchsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toProduction = [];
        $toProduction = DesignMaterial::toProduction($toProduction);
        $batches = OrderDetail::batches()->get();
        $details = OrderDetail::batchDetails()->get();
        $stocks = Product::join('designs', 'designs.id', 'design_id')
            ->select('design_id', DB::raw('SUM(stock) as stock'))
            ->groupBy('design_id')
            ->get();
        $actives = Batch::join('batch_details', 'batches.id', 'batch_id')
            ->select('batches.id as id', 'startDate', 'design_id', DB::raw('SUM(status) /COUNT(status) as progress'))
            ->groupBy('id', 'startDate', 'design_id')
            ->orderBy('startDate')->get();


        return view('backend.batches.index', compact('details', 'toProduction', 'batches', 'stocks', 'actives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        session(['batchDetails' => []]);

        $date = date('Y-m-d');
        $design = Design::find(session('design_id'));
        $details = OrderDetail::batchDetails()->where('design_id', session('design_id'))->get();
        foreach ($details as $detail){
            $bd = new BatchDetail();
            $bd->product_id = $detail->product_id;
            $bd->quantity = ($detail->total - $detail->stock);
            if($bd->quantity > 0){
                session()->push('batchDetails', $bd);
            }

        }
        $batchDetails = session('batchDetails');
        return view('backend.batches.create', compact('design', 'date', 'batchDetails'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $batches = OrderDetail::batches()->get();
        $details = OrderDetail::batchDetails()->get();
        $toProduction = [];
        $toProduction = DesignMaterial::toProduction($toProduction);


        if($request->has('deliver')){
            $orders = Order::join('order_details', 'orders.id', 'order_details.order_id')
                ->join('products', 'products.id', 'order_details.product_id')
                ->join('designs', 'designs.id', 'products.design_id')
                ->where('orders.status', 'ordered')
                ->where('design_id', $request->design_id)->get();

            //$total = $orders->select(DB::raw('SUM(quantity) as total'))->first()->total;
            dd($orders);
        }
        else if($request->has('batch')){
            return redirect('batch/create')->with('design_id', $request->design_id);
        }
        else if($request->has('purchase')){
            session(['purchaseDetails' => []]);
            $totalProducts = 0;
            foreach ($details as $detail) {
                if($detail->design_id == $request->design_id)
                    $totalProducts += ($detail->total - $detail->stock) > 0 ? ($detail->total - $detail->stock) : 0;
            }
            $design = Design::find($request->design_id);
            $design_materials = $design->details;
            foreach($design_materials as $dm){
                $material = Material::find($dm->material_id);
                $totalMaterial = ($dm->quantity * $totalProducts) - $material->stock;
                if($totalMaterial > 0){
                    $detail = new PurchaseDetail();
                    $detail->quantity = $totalMaterial;
                    $detail->material_id = $material->id;
                    $detail->subtotal = $totalMaterial * $material->price;
                    session()->push('purchaseDetails', $detail);
                }

            }

            return redirect('purchase/create');
        }
        else if($request->has('new')){
            $details = session('batchDetails');
            $batch = new Batch();
            $batch->design_id = $request->design_id;
            $batch->startDate = $request->startDate;
            $batch->save();
            foreach ($details as $detail){
                $detail->batch_id = $batch->id;
                $detail->save();
                $materials = $detail->product->design->materials;
                foreach($materials as $material){
                    foreach ($detail->product->design->details as $d){
                        if($material->id == $d->material_id){
                            $material->stock -= ($d->quantity * $detail->quantity);
                            $material->save();
                        }
                    }
                }
            }
            return redirect('batch')->with('success', 'Lote registrado exitosamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $batch = Batch::find($id);
        return view('backend.batches.edit', compact('batch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detail = BatchDetail::find($request->id);
        if($request->has("1"))
            $detail->status = 15;
        else if($request->has("2"))
            $detail->status = 25;
        else if($request->has("3"))
            $detail->status = 40;
        else if($request->has("4"))
            $detail->status = 45;
        else if($request->has("5"))
            $detail->status = 60;
        else if($request->has("6"))
            $detail->status = 80;
        else if($request->has("7")) {
            $detail->status = 100;
            $detail->product->stock += $detail->quantity;
            $detail->product->save();
        }

        $detail->save();
        return redirect()->back()->with('success', 'Progreso modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
