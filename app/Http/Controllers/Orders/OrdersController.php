<?php

namespace App\Http\Controllers\Orders;

use App\Customer;
use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    protected $rules = [
        'orderDate' => 'required',
        'estimatedDate' => 'required|after:orderDate'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['order_first' => true]);
        $orders = Order::orderBy('orderDate', 'desc')
            ->get();
        return view('backend.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('order_first')){
            session(['orderDetails' => [], 'order_first' => null]);
        }
        $details = session('orderDetails');
        $total = 0;
        foreach ($details as $detail){
            $total += $detail->subtotal;
        }
        $products = Product::all();
        $customers = Customer::all();
        return view('backend.orders.create', compact('products', 'customers', 'details', 'total'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $details = session('orderDetails');

        if($request->has('insert')){
            $this->validate($request, $this->rules);
            $order = new Order();
            $order->fill($request->all());
            $order->save();
            foreach ($details as $detail){
                $detail->order_id = $order->id;
                $detail->save();
            }
            session(['orderDetails' => []]);
            return redirect()->back()->with('success', 'Orden guardada exitosamente');
        }
        else if($request->has('add')){
            $this->validate($request, ['quantity' => 'required|numeric|integer|min:1']);
            $detail = new OrderDetail();
            $detail->quantity = $request->quantity;
            $detail->product_id = $request->product_id;
            $detail->subtotal = $detail->product->price * $request->quantity;
            foreach ($details as $d){
                if($d->product_id == $detail->product_id){
                    $d->quantity += $detail->quantity;
                    $d->subtotal = $d->quantity * $d->product->price;
                    session(['orderDetails' => $details]);
                    return redirect()->back()->withInput();
                }
            }
            session()->push('orderDetails', $detail);
            return redirect()->back()->withInput();
        } else if($request->has('quit')){
            unset($details[$request->id]);
            $details = array_values($details);
            session(['orderDetails' => $details]);
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $details = $order->details;
        $total = $order->total;
        $products = Product::all();
        $customers = Customer::all();
        return view('backend.orders.edit', compact('order', 'products', 'customers', 'details', 'total'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $total = 0;
        if($request->has('update')){
            $this->validate($request, $this->rules);
            $order->fill($request->all());
            $order->save();
            return redirect()->back()->with('success', 'Orden modificada exitosamente');
        }
        else if($request->has('add')){
            $this->validate($request, ['quantity' => 'required|numeric|integer|min:1']);
            $detail = new OrderDetail();
            $detail->quantity = $request->quantity;
            $detail->product_id = $request->product_id;
            $detail->subtotal = $detail->product->price * $request->quantity;
            foreach ($order->details as $d){
                if($d->product_id == $detail->product_id){
                    $d->quantity += $detail->quantity;
                    $d->subtotal = $d->quantity * $d->product->price;
                    $d->save();
                    $order = Order::find($id);
                    foreach ($order->details as $detail){
                        $total += $detail->subtotal;
                    }
                    $order->total = $total;
                    $order->save();
                    return redirect()->back()->withInput()->with('success', 'Detalle agregado exitosamente');
                }
            }
            $detail->order_id = $id;
            $detail->save();
            $order = Order::find($id);
            foreach ($order->details as $detail){
                $total += $detail->subtotal;
            }
            $order->total = $total;
            $order->save();
            return redirect()->back()->withInput()->with('success', 'Detalle agregado exitosamente');
        }
        else if($request->has('quit')){
            $detail = OrderDetail::find($request->id);
            $detail->delete();
            $order = Order::find($id);
            foreach ($order->details as $detail){
                $total += $detail->subtotal;
            }
            $order->total = $total;
            $order->save();
            return redirect()->back()->with('success', 'Detalle eliminado exitosamente');
        }
        else if($request->has('deliver')){
            $order = Order::find($id);
            $order->status = 'delivered';
            $details = $order->details;
            foreach ($details as $detail){
                $detail->product->stock -= $detail->quantity;
                $detail->product->save();
            }
            $order->save();
            return redirect()->back()->with('success', 'Estatus de pedido modificado exitosamente');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
