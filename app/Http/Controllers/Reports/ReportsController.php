<?php

namespace App\Http\Controllers\Reports;

use App\BatchDetail;
use App\Design;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Purchase;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{

    function index(){
        $details = OrderDetail::select('design_id', 'code', DB::raw('SUM(quantity) as total'))
            ->join('products', 'products.id', 'product_id')
            ->join('designs', 'designs.id', 'design_id')
            ->groupBy('design_id', 'code')
            ->get();

        $purchases = Purchase::select(DB::raw('SUM(total) as total, MONTH(purchaseDate) as mes'))
            ->whereBetween('purchaseDate', [(Carbon::now()->year . '-01-01'), (Carbon::now()->year . '-12-31')])
            ->groupBy(DB::raw('MONTH(purchaseDate)'))
            ->get();
        $purchasesData = [];
        for($i = 1; $i <= 12; $i++){
            $total = 0;
            foreach ($purchases as $purchase){
                if($i == $purchase->mes){
                    $total = $purchase->total;
                    continue;
                }
            }
            array_push($purchasesData, $total);
        }
        $orders = Order::select(DB::raw('SUM(total) as total, MONTH(orderDate) as mes'))
            ->whereBetween('orderDate', [(Carbon::now()->year . '-01-01'), (Carbon::now()->year . '-12-31')])
            ->groupBy(DB::raw('MONTH(orderDate)'))
            ->get();
        $ordersData = [];
        for($i = 1; $i <= 12; $i++){
            $total = 0;
            foreach ($orders as $order){
                if($i == $order->mes){
                    $total = $order->total;
                    continue;
                }
            }
            array_push($ordersData, $total);
        }

        $batchDetails = BatchDetail::select('status', DB::raw('SUM(quantity) as total'))
            ->groupBy('status')
            ->where('status', '<>', 100)
            ->get();
        return view('backend.reports.index', compact('details', 'purchasesData', 'ordersData', 'batchDetails'));
    }
}
