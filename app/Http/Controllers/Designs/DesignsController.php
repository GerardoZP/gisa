<?php

namespace App\Http\Controllers\Designs;

use App\Design;
use App\DesignMaterial;
use App\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DesignsController extends Controller
{
    protected $rules = [
        'name' => 'required|max:100',
        'code' => 'required|max:20'
    ];
    public $details = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $designs = Design::where(DB::raw('CONCAT(name, code)'),'LIKE','%'.$request->filter.'%')->get();
        return view('backend.designs.index', compact('designs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if(session('details') == null){
            session(['details' => []]);
        }
        $this->details = session('details');
        $details = $this->details;
        $materials = Material::all();
        return view('backend.designs.create', compact('materials','details'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if($request->has('insert')){
           $this->validate($request, $this->rules);
           $this->validate($request, ['code' => 'unique:designs,code']);
           $design = new Design();
           $design->fill($request->all());
           $design->save();
           $details = session('details');
           foreach ($details as $detail){
               $detail->design_id = $design->id;
               $detail->save();
           }
           session(['details' => []]);
           return redirect()->back()->with('success', 'Diseño guardado exitosamente');
       }
       else if($request->has('add')){
           $this->validate($request, ['quantity' => 'required|numeric|min:1', 'material_id' => 'required']);
           $detail = new DesignMaterial();
           $detail->quantity = $request->quantity;
           $detail->material_id = $request->material_id;
           $this->details = session('details');
           foreach ($this->details as $d){
               if($d->material_id == $detail->material_id){
                   $d->quantity += $detail->quantity;
                   session(['details' => $this->details]);
                   return redirect()->back()->withInput();
               }
           }
           session()->push('details', $detail);
           return redirect()->back()->withInput();
       } else if($request->has('quit')){
           $this->details = session('details');
           unset($this->details[$request->id]);
           $this->details = array_values($this->details);
           session(['details' => $this->details]);
           return redirect()->back()->withInput();
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $design = Design::find($id);
        $details = DesignMaterial::where('design_id', $id)->get();
        $materials = Material::all();
        session(['detailsEdit' => $details]);
        return view('backend.designs.edit', compact('design', 'details', 'materials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('update')){
            $this->validate($request, $this->rules);
            $design = Design::find($id);
            $design->fill($request->all());
            $design->status = $request->status ? 1 : 0;
            $design->save();
            return redirect()->back()->with('success', 'Diseño modificado exitosamente');
        }
        else if($request->has('add')){
            $this->validate($request, ['quantity' => 'required|numeric|min:1', 'material_id' => 'required']);
            $design = Design::find($id);
            foreach($design->details as $d){
                if($d->material_id == $request->material_id){
                    $d->quantity += $request->quantity;
                    $d->save();
                    return redirect()->back()->with('success', 'Detalle modificado exitosamente');
                }
            }
            $detail = new DesignMaterial();
            $detail->quantity = $request->quantity;
            $detail->material_id = $request->material_id;
            $detail->design_id = $id;
            $detail->save();
            return redirect()->back()->with('success', 'Detalle agregado exitosamente');
        }
        else if($request->has('quit')){
            $details = session('detailsEdit');
            $detail = $details[$request->id];
            $detail->delete();
            return redirect()->back()->with('success', 'Detalle eliminado exitosamente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
