<?php

namespace App\Http\Controllers\Purchases;

use App\Material;
use App\Provider;
use App\Purchase;
use App\PurchaseDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\URL;

class PurchasesController extends Controller
{

    protected $rules = [
        'purchaseDate' => 'required|date',
        'provider_id' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['first' => true]);
        $purchases = Purchase::orderBy('purchaseDate', 'DESC')->get();
        return view('backend.purchases.index', compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('first')){
            session(['purchaseDetails' => [], 'first' => null]);
        }
        $total = 0;
        $details = session('purchaseDetails');
        foreach($details as $detail){
            $total += $detail->subtotal;
        }
        $providers = Provider::where('status', 1)->get();
        $materials = Material::all();
        return view('backend.purchases.create', compact('providers', 'materials', 'details', 'total'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('insert')){
            $this->validate($request, $this->rules);
            $purchase = new Purchase();
            $purchase->fill($request->all());
            $purchase->save();
            $details = session('purchaseDetails');
            foreach ($details as $detail){
                $detail->purchase_id = $purchase->id;
                $detail->save();
                $detail->material->stock += $detail->quantity;
                $detail->material->save();
            }
            session(['purchaseDetails' => []]);

            return redirect()->back()->with('success', 'Compra registrada exitosamente');

        }
        else if($request->has('add')){
            $this->validate($request, ['quantity' => 'required|numeric|integer|min:1']);
            $detail = new PurchaseDetail();
            $detail->quantity = $request->quantity;
            $detail->material_id = $request->material_id;
            $detail->subtotal = $request->quantity * Material::find($request->material_id)->price;
            $details = session('purchaseDetails');
            foreach ($details as $d){
                if($d->material_id == $detail->material_id){
                    $d->quantity += $detail->quantity;
                    $d->subtotal += $detail->subtotal;
                    session(['purchaseDetails' => $details]);
                    return redirect()->back()->withInput();
                }
            }
            session()->push('purchaseDetails', $detail);
            return redirect()->back()->withInput();
        } else if($request->has('quit')){
            $details = session('purchaseDetails');
            unset($details[$request->id]);
            $details = array_values($details);
            session(['purchaseDetails' => $details]);
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchase = Purchase::find($id);

        if(session('first')){
            $details = $purchase->details;
            session(['purchaseDetailsEdit' => $details, 'first' => null]);
        }
        else{
            $details = session('purchaseDetailsEdit');
        }
        $providers = Provider::all();
        $materials = Material::all();
        $total = $purchase->total;
        return view('backend.purchases.edit', compact('purchase', 'details', 'providers', 'materials', 'total'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('update')){
            $this->validate($request, $this->rules);
            $purchase = Purchase::find($id);
            $purchase->fill($request->all());
            $purchase->save();
            return redirect()->back()->with('success', 'Compra modificada exitosamente');
        }
        else if($request->has('add')){
            $this->validate($request, ['quantity' => 'required|numeric|integer|min:1']);
            $detail = new PurchaseDetail();
            $detail->quantity = $request->quantity;
            $detail->material_id = $request->material_id;
            $detail->subtotal = $request->quantity * Material::find($request->material_id)->price;
            $detail->purchase_id = $id;
            $details = session('purchaseDetailsEdit');
            foreach ($details as $d){
                if($d->material_id == $detail->material_id){
                    $d->quantity += $detail->quantity;
                    $d->subtotal += $detail->subtotal;
                    $d->save();
                    $d->material->stock += $d->quantity;
                    $d->material->save();
                    $purchase = Purchase::find($request->purchase_id);

                    $total = 0;
                    foreach($purchase->details as $detail){
                        $total += $detail->subtotal;
                    }
                    $purchase->total = $total;
                    $purchase->save();
                    return redirect()->back()->withInput();
                }
            }
            $detail->save();
            $detail->material->stock += $detail->quantity;
            $detail->material->save();

            $purchase = Purchase::find($request->purchase_id);

            $total = 0;
            foreach($purchase->details as $detail){
                $total += $detail->subtotal;
            }
            $purchase->total = $total;
            $purchase->save();
            session()->push('purchaseDetailsEdit', $detail);
            return redirect()->back()->withInput();
        }
        else if($request->has('quit')){
            $details = session('purchaseDetailsEdit');
            $detail = $details[$request->id];
            $detail->delete();
            $total = 0;
            $purchase = Purchase::find($request->purchase_id);

            foreach($purchase->details as $detail){
                $total += $detail->subtotal;
            }
            $details = $purchase->details;
            session(['purchaseDetailsEdit' => $details]);


            $purchase->total = $total;
            $purchase->save();
            return redirect()->back()->with('success', 'Detalle eliminado exitosamente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
