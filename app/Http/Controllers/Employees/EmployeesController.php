<?php

namespace App\Http\Controllers\Employees;

use App\Employees;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EmployeesController extends Controller
{
     protected $rules = [
         'name' => 'required|max:50',
         'firstname' => 'required|max:70',
         'lastname' => 'required|max:70'
     ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$employees = Employees::all();
        $employees = Employees::where(DB::raw('CONCAT(name,firstname, lastname)'),'LIKE','%'.$request->filter.'%')->get();
        return view('backend.employees.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $employees = new Employees();
        $employees->fill($request->all());
        $employees->save();
        return redirect()->back()->with('success','Empleado guardado éxitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function show(Employees $employees)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employees::find($id);
        return view('backend.employees.edit',compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);
        $employees = Employees::find($id);
        $employees->fill($request->all());
        $employees->save();
        return redirect()->back()->with('success','Empleado modificado éxitorsamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employees::destroy($id);
        return redirect()->back()->with('success', 'Empleado eliminado exitosamente');
    }
}
