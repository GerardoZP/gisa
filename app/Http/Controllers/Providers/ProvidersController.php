<?php

namespace App\Http\Controllers\Providers;

use App\Provider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProvidersController extends Controller
{
    protected $rules =[
        'name' => 'required|max:100',
        'rfc' => 'required|max:25',
        'telephone' => 'required|numeric',
        'email' => 'required|unique:providers'
    ];

    protected $rulesEdit =[
        'name' => 'required|max:100',
        'rfc' => 'required|max:25',
        'telephone' => 'required|numeric',
        'email' => 'required'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $providers = Provider::where(DB::raw('CONCAT(name ,rfc)'),'LIKE', '%'. $request->filter  .'%')->get();
        return view('backend.providers.index',compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $provider = new Provider();
        $provider->fill($request->all());
        $provider->save();
        return redirect()->back()->with('success','Proveedor guardado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = Provider::find($id);
        return view('backend.providers.edit',compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rulesEdit );
        $provider = Provider::find($id);
        $provider->fill($request->all());
        $provider->save();
        return redirect()->back()->with('success','Proveedor modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        if ($request->action == "desactivar"){
            DB::table('providers')->where('id', $id)->update(['status' => False]);
            return redirect()->back()->with('success','Proveedor desactivado éxitosamente');
        }else if($request->action == "activar"){
            DB::table('providers')->where('id', $id)->update(['status' => True]);
            return redirect()->back()->with('success','Proveedor activado éxitosamente');
        }
    }


}
