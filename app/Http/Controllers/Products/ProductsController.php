<?php

namespace App\Http\Controllers\Products;

use App\Design;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    protected $rules =[
        'size' => 'required|max:100',
        'stock' => 'required|integer|min:0'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $designs = Design::where(DB::raw('CONCAT(name,code)'),'LIKE','%'.$request->filter.'%')
            ->get();
        return view('backend.products.index',compact('designs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designs = Design::all();
        return view('backend.products.create',compact('designs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $details = Design::find($request->design_id)->details;
        $cost = 0;
        foreach ($details as $detail){
            $cost += $detail->material->price * $detail->quantity;
        }
        $this->validate($request, ['price' => 'numeric|required|min:' . $cost]);
        $products = new Product();
        $products->fill($request->all());
        $products->save();
        return redirect()->back()->with('success','Producto guardado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $designs = Design::all();
        return view('backend.products.edit',compact('product','designs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);
        $products = Product::find($id);
        $products->fill($request->all());
        $products->save();
        return redirect()->back()->with('success','Producto guardado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
