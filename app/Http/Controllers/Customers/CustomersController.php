<?php

namespace App\Http\Controllers\Customers;

use App\Customer;
use App\Zone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CustomersController extends Controller
{
    protected $rules = [
        'name' => 'required|max:100',
        'firstname' => 'required|max:100',
        'lastname' => 'required|max:100',
        'city' => 'required|max:50',
        'email' => 'required|max:255',
        'rfc' => 'max:25',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers = Customer::where(DB::raw('CONCAT(name, firstname, lastname)'), 'LIKE', '%' . $request->filter  . '%')->get();
        return view('backend.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zones = Zone::all();
        return view('backend.customers.create', compact('zones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $customer = new Customer();
        $customer->fill($request->all());
        $customer->save();
        return redirect()->back()->with('success', 'Cliente guardado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $zones = Zone::all();
        return view('backend.customers.edit', compact('customer','zones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);
        $customer = Customer::find($id);
        $customer->fill($request->all());
        $customer->save();
        return redirect()->back()->with('success', 'Cliente modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::destroy($id);
        return redirect()->back()->with('success', 'Cliente eliminado exitosamente');
    }
}
