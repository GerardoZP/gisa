<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = [
        'purchaseDate',
        'total',
        'provider_id'
    ];
    public function provider(){
        return $this->belongsTo('App\Provider');
    }
    public function details(){
        return $this->hasMany('App\PurchaseDetail');
    }
}
