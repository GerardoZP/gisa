<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name',
        'lastname',
        'firstname',
        'city',
        'email',
        'rfc',
        'zone_id'
    ];

    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function zone(){
        return $this->belongsTo('App\Zone');
    }
}
