<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable =[
        'size','stock','price','design_id'
    ];

    public function design(){
        return $this->belongsTo('App\Design');
    }

    public function orderDetails(){
        return $this->hasMany('App\OrderDetail');
    }
}
