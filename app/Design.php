<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    protected $fillable = [
        'name',
        'code'
    ];

    public function materials(){
        return $this->belongsToMany('App\Material', 'design_materials');
    }

    public function details()
    {
        return $this->hasMany('App\DesignMaterial');
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }

}
