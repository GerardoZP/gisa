<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        "name","rfc","telephone", "email"
    ];

    public function purchases(){
        return $this->hasMany('App\Purchase');
    }
}
