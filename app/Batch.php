<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public function design(){
        return $this->belongsTo('App\Design');
    }
    public function details(){
        return $this->hasMany('App\BatchDetail');
    }
}
