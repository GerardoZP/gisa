<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    public function material(){
        return $this->belongsTo('App\Material');
    }
    public function purchase(){
        return $this->belongsTo('App\Purchase');
    }
}
