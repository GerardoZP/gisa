<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignMaterial extends Model
{
    public function material(){
        return $this->belongsTo('App\Material');
    }
    public function design(){
        return $this->belongsTo('App\Design');
    }
    public static function toProduction($toProduction){
        $design_materials = DesignMaterial::all();
        foreach ($design_materials as $design_material){
            $new = true;
            $design_id = $design_material->design_id;
            $capacity = floor($design_material->material->stock / $design_material->quantity);
            foreach ($toProduction as $t => $subArr){
                if($design_id == $t['id']){
                    $new = false;
                    if($capacity < $t['capacity']){
                        $t['capacity'] = $capacity;
                        break;
                    }
                }
            }
            $p = ['id' => $design_id , 'capacity' => $capacity];
            if($new){
                array_push($toProduction, $p);
            }
        }
        return $toProduction;
    }
}
