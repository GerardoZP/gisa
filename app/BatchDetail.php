<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchDetail extends Model
{
    protected $fillable = [
        'startDate',
        'design_id'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function batch(){
        return $this->belongsTo('App\Batch');
    }
}
