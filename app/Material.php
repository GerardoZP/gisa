<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{

    protected $fillable=[
      'name','code','stock','price'
    ];

    public function designs(){
        return $this->belongsToMany('App\Design', 'design_materials');
    }

    public function details(){
        return $this->hasMany('App\DesignMaterial');
    }

    public function purchaseDetails(){
        return $this->hasMany('App\PurchaseDetail');
    }

}
