<?php

namespace App\Providers;

use App\Batch;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $actives = [];

        try{
            $actives = Batch::join('batch_details', 'batches.id', 'batch_id')
                ->select('batches.id as id', 'startDate', 'design_id', DB::raw('SUM(status) /COUNT(status) as progress'))
                ->groupBy('id', 'startDate', 'design_id')
                ->orderBy('startDate')
                ->where('status', '<>', 100)
                ->get();
        }catch(QueryException $e){

        }

        View::share('activeBatches', $actives);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
