<?php

use Illuminate\Database\Seeder;

class DesignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designs = factory(\App\Design::class, 10)->make();
        foreach($designs as $design){
            repeat:
            try{
                $design->save();
            }catch (\Illuminate\Database\QueryException $e){
                $design = factory(\App\Design::class)->make();
                goto repeat;
            }
        }
    }
}
