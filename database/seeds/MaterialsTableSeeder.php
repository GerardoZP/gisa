<?php

use Illuminate\Database\Seeder;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materials = factory(\App\Material::class, 100)->make();
        foreach ($materials as $material){
            repeat:
            try{
                $material->save();
            } catch (\Illuminate\Database\QueryException $e){
                $material = factory(\App\Material::class)->make();
                goto repeat;
            }
        }

        $design_materials = factory(\App\DesignMaterial::class, 120)->make();
        foreach ($design_materials as $design_material){
            repeat_detail:
            try{
                $design_material->save();
            }catch (\Illuminate\Database\QueryException $e){
                $design_material = factory(\App\DesignMaterial::class)->make();
                goto repeat_detail;
            }
        }
    }
}
