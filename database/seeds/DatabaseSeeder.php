<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ZonesTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(ProvidersTableSeeder::class);
        $this->call(DesignsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(MaterialsTableSeeder::class);
        $this->call(PurchasesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
    }
}
