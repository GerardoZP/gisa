<?php

use Illuminate\Database\Seeder;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zones = factory(\App\Zone::class, 5)->make();
        foreach ($zones as $zone){
            repeat:
            try{
                $zone->save();
            } catch (\Illuminate\Database\QueryException $e){
                $purchase = factory(\App\Zone::class)->make();
                goto repeat;
            }
        }

    }
}
