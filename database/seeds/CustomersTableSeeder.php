<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $customers = factory(\App\Customer::class, 50)->make();
        foreach($customers as $customer){
            repeat:
            try{
                $customer->save();
            } catch(\Illuminate\Database\QueryException $e){
                $customer = factory(\App\Customer::class)->make();
                goto repeat;
            }
        }
    }
}
