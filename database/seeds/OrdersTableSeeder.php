<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = factory(\App\Order::class, 50)->make();
        foreach ($orders as $order){
            repeat:
            try{
                $order->save();
            } catch (\Illuminate\Database\QueryException $e){
                $order = factory(\App\Order::class)->make();
                goto repeat;
            }
        }

        $order_details = factory(\App\OrderDetail::class, 120)->make();
        foreach ($order_details as $order_detail){
            repeat_detail:
            try{
                $order_detail->save();
            }catch (\Illuminate\Database\QueryException $e){
                $order_detail = factory(\App\OrderDetail::class)->make();
                goto repeat_detail;
            }
        }
    }
}
