<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $employees = factory(\App\Employees::class,10)->make();
        foreach ($employees as $employee) {
            repeat:
            try{
                $employee->save();
            }catch (\Illuminate\Database\QueryException $e){
                $employee= factory(\App\Employees::class)->make();
                goto repeat;
            }
        }

    }
}
