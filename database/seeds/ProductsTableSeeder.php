<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = factory(\App\Product::class, 100)->make();
        foreach ($products as $product){
            repeat:
            try{
                $product->save();
            } catch (\Illuminate\Database\QueryException $e){
                $product = factory(\App\Product::class)->make();
                goto repeat;
            }
        }
    }
}
