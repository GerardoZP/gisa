<?php

use Illuminate\Database\Seeder;

class ProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $providers = factory(\App\Provider::class, 10)->make();
        foreach($providers as $provider){
            repeat:
            try{
                $provider->save();
            }catch (\Illuminate\Database\QueryException $e){
                $provider = factory(\App\Provider::class)->make();
                goto repeat;
            }
        }
    }
}
