<?php

use Illuminate\Database\Seeder;

class PurchasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchases = factory(\App\Purchase::class, 50)->make();
        foreach ($purchases as $purchase){
            repeat:
            try{
                $purchase->save();
            } catch (\Illuminate\Database\QueryException $e){
                $purchase = factory(\App\Purchase::class)->make();
                goto repeat;
            }
        }

        $purchase_details = factory(\App\PurchaseDetail::class, 120)->make();
        foreach ($purchase_details as $purchase_detail){
            repeat_detail:
            try{
                $purchase_detail->save();
            }catch (\Illuminate\Database\QueryException $e){
                $purchase_detail = factory(\App\PurchaseDetail::class)->make();
                goto repeat_detail;
            }
        }
    }
}
