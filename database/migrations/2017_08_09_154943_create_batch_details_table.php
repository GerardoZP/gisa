<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('batch_id')->unsigned();
            $table->integer('quantity');
            //$table->enum('status', ['programado', 'corte', 'preliminar', 'pespunte', 'gancho', 'montado', 'adorno', 'terminado'])->default('programado');
            //$table->enum('status', [0, 15, 25, 40, 45, 60, 80, 100])->default(0);
            $table->integer('status')->default(0);
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('batch_id')->references('id')->on('batches');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_details');
    }
}
