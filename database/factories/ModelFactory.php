<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
   return [
       'name' => $faker->firstName,
       'firstname' => $faker->lastName,
       'lastname' => $faker->lastName,
       'city' => $faker->city,
       'email' => $faker->safeEmail,
       'rfc' => $faker->bankAccountNumber,
       'zone_id' => $faker->numberBetween(1, 5)
   ];
});

$factory->define(App\Zone::class, function(Faker\Generator $faker){
    $zones = ['sur', 'norte', 'centro', 'noreste', 'noroeste', 'sureste', 'suroeste'];
    return [
        'name' => $zones[$faker->numberBetween(0, 6)]
    ];
});

$factory->define(App\Provider::class, function (Faker\Generator $faker){
   return [
       'name' => $faker->company,
       'rfc' => $faker->bankAccountNumber,
       'telephone' => $faker->phoneNumber,
       'email' => $faker->safeEmail
   ];
});

$factory->define(App\Design::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->safeColorName,
        'code' => $faker->postcode
    ];
});

$factory->define(App\Product::class, function(Faker\Generator $faker) {
   return [
       'size' => $faker->numberBetween(20, 30),
       'price' => $faker->randomFloat(2, 50, 1000),
       'stock' => $faker->numberBetween(1, 100),
       'design_id' => $faker->numberBetween(1, 100)
   ];
});

$factory->define(App\Material::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->userName,
        'code' => $faker->postcode,
        'stock' => $faker->randomDigit,
        'price' => $faker->randomFloat(2, 1, 500)
    ];
});

$factory->define(App\Purchase::class, function(Faker\Generator $faker){
    return[
        'status' => 'ordered',
        'purchaseDate' => $faker->date(),
        'total' => $faker->randomFloat(2, 50, 100000),
        'provider_id' => $faker->numberBetween(1, 10)
    ];
});

$factory->define(App\PurchaseDetail::class, function(Faker\Generator $faker){
    return [
        'subtotal' => $faker->randomFloat(2, 50, 1000),
        'quantity' => $faker->randomDigit,
        'purchase_id' => $faker->numberBetween(1, 50),
        'material_id' => $faker->numberBetween(1, 100)
    ];
});

$factory->define(App\Order::class, function(Faker\Generator $faker) {
   return [
       'status' => 'ordered',
       'orderDate' => $faker->dateTimeThisYear,
       'estimatedDate' => $faker->dateTimeThisYear,
       'total' => $faker->randomFloat(2, 50, 100000),
       'customer_id' => $faker->numberBetween(1, 100)
   ];
});

$factory->define(App\OrderDetail::class, function(Faker\Generator $faker){
    return [
        'subtotal' => $faker->randomFloat(2, 50, 1000),
        'quantity' => $faker->randomDigit,
        'order_id' => $faker->numberBetween(1, 100),
        'product_id' => $faker->numberBetween(1, 100)
    ];
});

$factory->define(App\DesignMaterial::class, function(Faker\Generator $faker){
    return [
        'quantity' => $faker->numberBetween(1, 15),
        'design_id' => $faker->numberBetween(1, 100),
        'material_id' => $faker->numberBetween(1, 100)

    ];
});

$factory->define(App\Employees::class,function(Faker\Generator $faker){
    return[
        'name' => $faker->name,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName
    ];
});