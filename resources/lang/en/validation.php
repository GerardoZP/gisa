<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => ':attribute debe ser una fecha posterior a :date.',
    'alpha'                => ':attribute solo debe contener letras.',
    'alpha_dash'           => ':attribute solo debe contener letras, números, y guiones.',
    'alpha_num'            => ':attribute solo debe contener letras y números.',
    'array'                => ':attribute debe ser un arreglo.',
    'before'               => ':attribute debe ser una fecha anterior a :date.',
    'between'              => [
        'numeric' => ':attribute debe estar entre :min y :max.',
        'file'    => ':attribute debe estar entre :min y :max kilobytes.',
        'string'  => ':attribute debe estar entre :min y :max caracteres.',
        'array'   => ':attribute debe tener entre :min y :max items.',
    ],
    'boolean'              => ':attribute debe tener un valor verdadero o falso.',
    'confirmed'            => 'La confirmación de :attribute no coincide.',
    'date'                 => ':attribute no es una fecha válida.',
    'date_format'          => ':attribute no corresponde al formato :format.',
    'different'            => ':attribute y :other deben ser diferentes.',
    'digits'               => ':attribute debe tener :digits dígitos.',
    'digits_between'       => ':attribute debe tener entre :min y :max dígitos.',
    'dimensions'           => ':attribute tiene dimensiones inválidas.',
    'distinct'             => ':attribute tiene un valor duplicado.',
    'email'                => ':attribute debe ser una dirección de correo válida.',
    'exists'               => ':attribute seleccionado es inválido.',
    'file'                 => ':attribute debe ser un archivo.',
    'filled'               => ':attribute es obligatorio.',
    'image'                => ':attribute debe ser una imagen.',
    'in'                   => ':attribute seleccionado es inválido.',
    'in_array'             => ':attribute no existe en :other.',
    'integer'              => ':attribute debe ser un número entero.',
    'ip'                   => ':attribute debe ser una dirección IP válida.',
    'json'                 => ':attribute debe ser un JSON válido.',
    'max'                  => [
        'numeric' => ':attribute no puede ser mayor que :max.',
        'file'    => ':attribute no puede ser mayor que :max kilobytes.',
        'string'  => ':attribute no puede ser mayor que :max caracteres.',
        'array'   => ':attribute no puede tener más de :max items.',
    ],
    'mimes'                => ':attribute debe ser un archivo de type: :values.',
    'mimetypes'            => ':attribute debe ser un archivo de type: :values.',
    'min'                  => [
        'numeric' => ':attribute al menos debe tener :min.',
        'file'    => ':attribute al menos debe tener :min kilobytes.',
        'string'  => ':attribute al menos debe tener :min caracteres.',
        'array'   => ':attribute al menos debe tener :min items.',
    ],
    'not_in'               => ':attribute seleccionado es inválido.',
    'numeric'              => ':attribute debe ser numérico.',
    'present'              => ':attribute debe estar presente.',
    'regex'                => ':attribute formato es inválido.',
    'required'             => ':attribute es obligatorio.',
    'required_if'          => ':attribute es obligatorio cuando :other es :value.',
    'required_unless'      => ':attribute es obligatorio a no ser que :other está en :values.',
    'required_with'        => ':attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => ':attribute es obligatorio cuando :values están presentes.',
    'required_without'     => ':attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => ':attribute es obligatorio cuando ninguno de :values están presentes.',
    'same'                 => ':attribute y :other deben empatar.',
    'size'                 => [
        'numeric' => ':attribute deben tener :size.',
        'file'    => ':attribute deben tener :size kilobytes.',
        'string'  => ':attribute deben tener :size caracteres.',
        'array'   => ':attribute deben contener :size items.',
    ],
    'string'               => ':attribute debe ser una cadena de texto.',
    'timezone'             => ':attribute debe ser una zona válida.',
    'unique'               => ':attribute ya existe.',
    'uploaded'             => ':attribute falló al subir.',
    'url'                  => 'El formato de :attribute es inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'date_start' => [
            'before' => ':attribute debe ser una fecha anterior a mañana.',
        ],
        'date_end' => [
            'before' => ':attribute debe ser una fecha anterior a mañana.',
        ],
        'expenseDate' => [
            'before' => ':attribute debe ser una fecha anterior a mañana.',
        ],
        'purchaseDate' => [
            'before' => ':attribute debe ser una fecha anterior a mañana.',
        ],
        'paymentDate' => [
            'before' => ':attribute debe ser una fecha anterior a mañana.',
        ],
        'depositAmount' => [
            'max' => ':attribute debe ser menor o igual al monto de pago.',
        ],
        'saleDate' => [
          'before' => ':attribute debe ser una fecha anterior a mañana.',
        ],
        'price' => [
            'min' => ':attribute debe sobrepasar el costo de fabricación de :min'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        //Customer
        'name' => 'Nombre',
        'firstname' => 'Primer apellido',
        'lastname' => 'Segundo apellido',
        'city' => 'Ciudad',
        'email' => 'Correo electrónico',
        'rfc' => 'RFC',
        'code' => 'Código',
        'quantity' => 'Cantidad',
        'purchaseDate' => 'Fecha de compra',
        'orderDate' => 'Fecha de pedido',
        'estimatedDate' => 'Fecha estimada de entrega',
        'price' => 'Precio',
        'telephone' => 'Teléfono',
        'provider_id' => 'Proveedor',
        'material_id' => 'Material',
        'stock' => 'Stock',
        'password' => 'Contraseña'
    ],

];
