<div class="header-left">
    <div class="logo-name">
        <a href="{{route('home')}}">
            <img id="logo" src="{{asset('img/logo.jpg')}}" alt="Logo" width="70%"/>
        </a>
    </div>
    <!--search-box-->
    <div class="search-box">
        @yield('search')
    </div><!--//end-search-box-->
    <div class="clearfix"> </div>
</div>
<div class="header-right">
    <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
            <li class="dropdown head-dpdn">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">{{sizeof($activeBatches)}}</span></a>
                <ul class="dropdown-menu">
                    <li>
                        <div class="notification_header">
                            <h3>Tienes {{sizeof($activeBatches)}} lotes activos</h3>
                        </div>
                    </li>
                    @foreach($activeBatches as $batch)
                        <li><a href="{{route('batch.edit', $batch->id)}}">
                                <div class="task-info">
                                    <span class="task-desc">{{$batch->design->code}}</span><span class="percentage">{{$batch->progress}}%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar yellow" style="width:{{$batch->progress}}%;"></div>
                                </div>
                            </a></li>
                    @endforeach
                    <li>
                        <div class="notification_bottom">
                            <a href="{{url('batch')}}">Ver todos los lotes</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="clearfix"> </div>
    </div>
    <!--notification menu end -->
    <div class="profile_details">
        <ul>
            <li class="dropdown profile_details_drop">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <div class="profile_img">
                        <span class="prfil-img"><img src="images/p1.png" alt=""> </span>
                        <div class="user-name">
                            <span>Administrator</span>
                        </div>
                        <i class="fa fa-angle-down lnr"></i>
                        <i class="fa fa-angle-up lnr"></i>
                        <div class="clearfix"></div>
                    </div>
                </a>
                <ul class="dropdown-menu drp-mnu">
                    <li> <a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> Cerrar sesión</a> </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
<div class="clearfix"> </div>