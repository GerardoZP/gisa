<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    @include('generals.head')
</head>
<body>
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <!--header start here-->
            <div class="header-main">
                @include('generals.header')
            </div>

            <div class="inner-block">
                @include('generals.messages')
                @yield('content')
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <!--inner block end here-->
            <!--copy rights start here-->
            <div class="copyrights">
                <p>© 2017 GISA. All Rights Reserved | Developed by GISA</p>
            </div>
            <!--COPY rights end here-->
        </div>
    </div>
    <!--slider menu-->
    <div class="sidebar-menu">
        @include('generals.menu')
    </div>
    <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
@include('generals.js')
@yield('extra-js')
</body>
</html>