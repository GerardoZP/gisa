<div class="logo">
    <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span>
        <!--<img id="logo" src="" alt="Logo"/>-->
    </a>
</div>
<div class="menu">
    <ul id="menu" >
        <li id="menu-home" ><a href="{{route('home')}}"><i class="fa fa-tachometer"></i><span>Inicio</span></a></li>
        <li id="menu-home" ><a href="{{route('customer.index')}}"><i class="fa fa-user"></i><span>Clientes</span></a></li>
        <li id="menu-home" ><a href="{{route('employee.index')}}"><i class="fa fa-suitcase"></i><span>Empleados</span></a></li>
        <li id="menu-home" ><a href="{{route('zone.index')}}"><i class="fa fa-map"></i><span>Zonas</span></a></li>
        <li id="menu-home" ><a href="{{route('provider.index')}}"><i class="fa fa-automobile"></i><span>Proveedores</span></a></li>
        <li id="menu-home" ><a href="{{route('design.index')}}"><i class="fa fa-pencil-square-o"></i><span>Diseños</span></a></li>
        <li id="menu-home" ><a href="{{route('product.index')}}"><i class="fa fa-product-hunt"></i><span>Productos</span></a></li>
        <li id="menu-home" ><a href="{{route('material.index')}}"><i class="fa fa-gavel"></i><span>Materiales</span></a></li>
        <li id="menu-home" ><a href="{{route('purchase.index')}}"><i class="fa fa-shopping-bag"></i><span>Compras</span></a></li>
        <li id="menu-home" ><a href="{{route('order.index')}}"><i class="fa fa-shopping-cart"></i><span>Pedidos</span></a></li>
        <li id="menu-home" ><a href="{{route('reports')}}"><i class="fa fa-line-chart"></i><span>Reportes</span></a></li>
        <li id="menu-home" ><a href="{{route('batch.index')}}"><i class="fa fa-file-text-o"></i><span>Lotes</span></a></li>
    </ul>
</div>
