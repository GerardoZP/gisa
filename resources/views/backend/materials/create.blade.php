@extends('generals.main')
@section('content')
    {{Form::open(['route'=> 'material.store','class'=> 'form','method'=>'POST'])}}
        @include('backend.materials.partials.inputs')
        <input type="submit" value="Guardar" class="btn btn-lg btn-success">
    {{Form::close()}}
@endsection