@extends('generals.main')
@section('content')
    <h1>Material</h1>

    <div class="card">
        {{Form::open(['route' => ['material.update', $materials->id], 'class' => 'form', 'method' => 'PUT'])}}
        @include('backend.materials.partials.inputs')
            <input type="submit" value="Guardar" class="btn btn-lg btn-success">
        {{Form::close()}}
    </div>
@endsection