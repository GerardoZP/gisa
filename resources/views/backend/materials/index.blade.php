@extends('generals.main')
@section('search')
    {{Form::open(['route' => 'material.index', 'method' => 'get'])}}
    <input type="text" placeholder="Busqueda..."  name="filter">
    <input type="submit" value="">
    {{Form::close()}}
@endsection
@section('content')
    <h1>Materiales</h1>
    <div class="col-lg-6">
        <br> <a href="{{route('material.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br></a>
    </div>
    @include('backend.materials.partials.table')
    @endsection