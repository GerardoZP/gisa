<div class="row">
    <div class="col-lg-4 form-group">
        <label>Nombre del Material</label>
        <input type="text" name="name" placeholder="Ingresa el Nombre del Material" class="form-control" value="{{isset($materials) ? $materials->name : old('name')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Código del Material</label>
        <input type="text" name="code" placeholder="Código del material" class="form-control" value="{{isset($materials) ? $materials->code : old('name')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Stock</label>
        <input type="text" name="stock" placeholder="Ingresa la Cantindad en inventario" class="form-control" value="{{isset($materials) ? $materials->stock : old('stock')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Precio del material</label>
        <input type="text" name="price" placeholder="Ingresa el precio" class="form-control" value="{{isset($materials) ? $materials->price : old('price')}}">
    </div>
</div>