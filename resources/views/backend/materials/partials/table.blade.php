<table class="table table-responsive">
    <tr>

        <th>ID</th>
        <th>Código</th>
        <th>Nombre</th>
        <th>Stock</th>
        <th>Precio</th>
        <th></th>

    </tr>

        @foreach($materials as $material)
        <tr>
            <td>{{$material->id}}</td>
            <td>{{$material->code}}</td>
            <td>{{$material->name}}</td>
            <td>{{$material->stock}}</td>
            <td>{{$material->price}}</td>
            <td>
                {{Form::open(['route' => ['material.edit', $material->id], 'method' => 'get'])}}
                    <input type="submit"  value="Editar" class="btn btn-info">
                {{Form::close()}}
            </td>
        </tr>
    @endforeach
</table>