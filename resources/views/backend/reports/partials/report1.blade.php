<div class="col-md-6 chart-layer1-left">
    <div class="glocy-chart">
        <div class="span-2c">
            <h3 class="tlt">Entradas y salidas por mes</h3>
            <canvas id="bar" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
            <script>
                var barChartData = {
                    labels : ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago", "Sept", "Oct", "Nov", "Dic"],
                    datasets : [
                        {
                            fillColor : "#FC8213",
                            data : [
                                @for($i = 0; $i < sizeof($purchasesData); $i++)
                                    {{$purchasesData[$i]}},
                                @endfor
                            ]
                        },
                        {
                            fillColor : "#337AB7",
                            data : [
                                @for($i = 0; $i < sizeof($ordersData); $i++)
                                    {{$ordersData[$i]}},
                                @endfor
                            ]
                        }
                    ]

                };
                new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);

            </script>
        </div>
    </div>
</div>