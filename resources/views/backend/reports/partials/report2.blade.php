<div class="col-md-6 chart-blo-1">
    <div class="chart-other">
        <h3>Piezas en producción por proceso</h3>
        <canvas id="pie" height="315" width="470" style="width: 470px; height: 315px;"></canvas>
        <script>
            var pieData = [
                @foreach($batchDetails as $detail)
                {
                    label:
                        @if($detail->status == 0) 'Programados',
                        @elseif($detail->status == 15) 'En corte',
                        @elseif($detail->status == 25) 'En preeliminar',
                        @elseif($detail->status == 40) 'En pespunte',
                        @elseif($detail->status == 45) 'En gancho',
                        @elseif($detail->status == 60) 'En montado',
                        @elseif($detail->status == 80) 'En adorno',
                        @endif

                    value: '{{$detail->total}}',
                    color:
                            @if($detail->status == 0) '#FFA112',
                            @elseif($detail->status == 15) '#AFF1B2',
                            @elseif($detail->status == 25) '#650321',
                            @elseif($detail->status == 40) '#ABCDEF',
                            @elseif($detail->status == 45) '#12ABDF',
                            @elseif($detail->status == 60) '#FFFCCC',
                            @elseif($detail->status == 80) '#A10F0A',
                            @endif
                },
                @endforeach
            ];
            new Chart(document.getElementById("pie").getContext("2d")).Pie(pieData);
        </script>
    </div>
</div>