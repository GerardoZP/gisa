<div class="col-md-12 chart-layer2-right">
    <div class="prograc-blocks">
        <!--Progress bars-->
        <div class="home-progres-main">
            <h3>Total de órdenes por estilo</h3>
        </div>
        <div class='bar_group'>
            @foreach($details as $detail)
                <div class='bar_group__bar thin' label='Diseño: {{$detail->code}}' show_values='true' tooltip='true' value='{{$detail->total}}'></div>
            @endforeach
        </div>
        <script src="js/bars.js"></script>

        <!--//Progress bars-->
    </div>
</div>