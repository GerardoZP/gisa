@extends('generals.main')
@section('content')
    <h1>Reportes</h1>
    <div class="main-page-charts">
        <div class="main-page-chart-layer1">
            @include('backend.reports.partials.report1')
            @include('backend.reports.partials.report2')

            @include('backend.reports.partials.report3')
         <div class="clearfix"></div>

        </div>
    </div>
@endsection