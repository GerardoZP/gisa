@extends('generals.main')
@section('content')
    <h1>Pedidos</h1>
    <div class="col-lg-6">
        <br><a href="{{route('order.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br>
    </div>
    @include('backend.orders.partials.table')
    <div class="clearfix"></div>
@endsection