@extends('generals.main')
@section('content')
    <h1>Ingresar Pedido</h1>
    {{Form::open(['route' => 'order.store', 'method' => 'post'])}}
        @include('backend.orders.partials.inputs')
        <div class="row">
            <div class="col-lg-4">
                <select class="form-control" name="product_id">
                    @foreach($products as $product)
                        <option value="{{$product->id}}">{{$product->design->name}} - {{$product->size}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <input type="text"  class="form-control" name="quantity" placeholder="Cantidad">
            </div>
            <div class="col-lg-4">
                <input type="submit" name="add" value="Agregar" class="btn btn-primary">
            </div>
            @include('backend.orders.partials.tableDetails')
            <div class="row">
                <div class="col-lg-4">
                    <label>Total</label>
                    <input type="text" value="{{$total}}" class="form-control" name="total" readonly>
                </div>
            </div>
            <input type="submit" name="insert" value="Guardar" class="btn btn-success">

        </div>
    {{Form::close()}}
@endsection