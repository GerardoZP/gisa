<table class="table table-responsive">
    <tr>
        <th>Producto</th>
        <th>Precio unitario</th>
        <th>Cantidad</th>
        <th>Subtotal</th>
        <th>Quitar</th>
    </tr>
    @for($i = 0; $i < sizeof($details); $i++)
        <tr>
            <td>{{$details[$i]->product->design->name}} - {{$details[$i]->product->size}}</td>
            <td>{{$details[$i]->product->price}}</td>
            <td>{{$details[$i]->quantity}}</td>
            <td>{{$details[$i]->subtotal}}</td>

            <td>
                @if(Route::currentRouteName() == 'order.create')
                    {{Form::open(['route' =>  'order.store', 'method' => 'post'])}}
                    <input type="hidden" name="id" value="{{$i}}">
                    <input type="submit" class="btn btn-danger" name="quit" value="Quitar">
                @else
                    {{Form::open(['route' =>  ['order.update', $i], 'method' => 'put', 'name' => 'quit_form'])}}
                    <input type="hidden" name="id" value="{{$details[$i]->id}}">
                    <input type="submit" class="btn btn-danger" value="Quitar" name="quit" >
                @endif
                {{Form::close()}}
            </td>
        </tr>
    @endfor
</table>