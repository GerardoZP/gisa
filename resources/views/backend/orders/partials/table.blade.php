<table class="table table-responsive">
    <tr>
        <th>Fecha de pedido</th>
        <th>Fecha estimada</th>
        <th>Cliente</th>
        <th>Total</th>
        <th>Estatus</th>
        <th>Editar</th>
        <th>Entrega</th>
    </tr>
    @foreach($orders as $order)@php $complete = true; @endphp
        @foreach($order->details as $detail)
            @if($detail->quantity > $detail->product->stock)
                @php $complete = false; @endphp
            @endif
        @endforeach
        <tr>
            <td>{{$order->orderDate}}</td>
            <td>{{$order->estimatedDate}}</td>
            <td>{{$order->customer->name}} {{$order->customer->firstname}}</td>
            <td>{{$order->total}}</td>
            <td>
                @if($order->status == 'ordered')
                    <span class="label label-info">Pendiente</span>
                @else
                    <span class="label label-success">Entregada</span>
                @endif

            </td>
            <td>
                @if($order->status == 'ordered')
                        {{Form::open(['route' => ['order.edit', $order->id], 'method' => 'get'])}}
                        <input type="submit" value="Editar" class="btn btn-info">
                        {{Form::close()}}
                @endif

            </td>
            <td>


                @if($order->status == 'ordered')
                    @if($complete)
                        {{Form::open(['route' => ['order.update', $order->id], 'method' => 'put'])}}
                        <input type="submit" name="deliver" value="Entregar" class="btn btn-success">
                        {{Form::close()}}
                    @else
                        {{Form::open(['route' => 'batch.index', 'method' => 'get'])}}
                        <input type="submit"  value="Ir a lotes" class="btn btn-warning">
                        {{Form::close()}}
                    @endif
                @endif
            </td>
        </tr>
    @endforeach
</table>