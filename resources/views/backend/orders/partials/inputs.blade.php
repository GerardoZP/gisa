<div class="row">
    <div class="col-lg-4 form-group">
        <label>Fecha de pedido</label>
        <input type="date" name="orderDate" placeholder="Fecha" class="form-control" value="{{isset($order) ? $order->orderDate : old('orderDate')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Fecha estimada de entrega</label>
        <input type="date" name="estimatedDate" placeholder="Fecha" class="form-control" value="{{isset($order) ? $order->estimatedDate : old('estimatedDate')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Cliente</label>
        <select class="form-control" name="customer_id">
            @foreach($customers as $customer)
                <option
                        value="{{$customer->id}}"
                        {{isset($order) ?
                        $order->customer_id == $customer->id ? 'selected' : '' :
                         \Illuminate\Support\Facades\Input::old('customer_id') == $customer->id ? 'selected' : ''}}
                >
                    {{$customer->id}} {{$customer->name}} {{$customer->firstname}}
                </option>
            @endforeach
        </select>
    </div>
</div>