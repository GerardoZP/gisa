@extends('generals.main')
@section('search')
    {{Form::open(['route' => 'design.index', 'method' => 'get'])}}
    <input type="text" placeholder="Busqueda..."  name="filter">
    <input type="submit" value="">
    {{Form::close()}}
@endsection
@section('content')
    <h1>Diseños</h1>
    <div class="col-lg-6">
       <br><a href="{{route('design.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br>
    </div>
    @include('backend.designs.partials.table')
@endsection
