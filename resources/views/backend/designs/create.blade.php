@extends('generals.main')
@section('content')
    <h1>Ingresar Diseño</h1>

    {{Form::open(['route' => 'design.store', 'class' => 'form', 'mehtod' => 'POST'])}}
        @include('backend.designs.partials.inputs')
        <div class="row">
            <div class="col-lg-4">
                <select class="form-control" name="material_id">
                    @foreach($materials as $material)
                        <option value="{{$material->id}}">{{$material->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <input type="text"  class="form-control" name="quantity" placeholder="Cantidad">
            </div>
            <div class="col-lg-4">
                <input type="submit" name="add" value="Agregar" class="btn btn-primary">
            </div>
            @include('backend.designs.partials.tableDetails')
        </div>







    <input type="submit" name="insert" value="Guardar" class="btn btn-lg btn-success">
    {{Form::close()}}


@endsection