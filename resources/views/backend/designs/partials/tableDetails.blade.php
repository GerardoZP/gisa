<table class="table table-responsive">
    <tr>
        <th>Material</th>
        <th>Cantidad</th>
        <th>Quitar</th>
    </tr>
    @for($i = 0; $i < sizeof($details); $i++)
        <tr>
            <td>{{$details[$i]->material->name}}</td>
            <td>{{$details[$i]->quantity}}</td>

            <td>
                @if(Route::currentRouteName() == 'design.create')
                    {{Form::open(['route' =>  'design.store', 'method' => 'post'])}}
                        <input type="hidden" name="id" value="{{$i}}">
                        <input type="submit" class="btn btn-danger" name="quit" value="Quitar">
                @else
                    {{Form::open(['route' =>  ['design.update', $i], 'method' => 'put', 'name' => 'quit_form'])}}
                        <input type="hidden" name="id" value="{{$i}}">
                        <input type="submit" class="btn btn-danger" value="Quitar" name="quit" >
                @endif
                    {{Form::close()}}
            </td>
        </tr>
    @endfor
</table>