<table class="table table-responsive">
    <tr>
        <th>ID</th>
        <th>Linea</th>
        <th>Código</th>
        <th>Costo de fabricación</th>
        <th>Estatus</th>
        <th>Editar</th>
    </tr>
    @foreach($designs as $design)
        <tr>
            <td>{{$design->id}}</td>
            <td>{{$design->name}}</td>
            <td>{{$design->code}}</td>
            <td>
                @php $cost = 0; @endphp
                @foreach($design->details as $detail)
                    @php $cost += $detail->material->price * $detail->quantity; @endphp
                @endforeach
                {{$cost}}
            </td>
            <td>{{$design->status ? 'Activo' : 'Inactivo'}}</td>
            <td>
                {{Form::open(['route' => ['design.edit', $design->id], 'method' => 'get'])}}
                    <input type="submit"  value="Editar" class="btn btn-info">
                {{Form::close()}}
            </td>
        </tr>
    @endforeach
</table>