<div class="row">
    <div class="col-lg-4 form-group">
        <label>Nombre</label>
        <input type="text" name="name" placeholder="Nombre" class="form-control" value="{{isset($design) ? $design->name : old('name')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Código</label>
        <input type="text" name="code" placeholder="code" class="form-control" value="{{isset($design) ? $design->code : old('code')}}">
    </div>
    @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'design.edit')
        <div class="col-lg-4 form-group">
            <label>Estatus</label>
            <input type="checkbox" name="status" class="form-control" {{isset($design) ? $design->status ? 'checked' : '' : ''}}>
        </div>
    @endif
</div>
