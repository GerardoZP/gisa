@extends('generals.main')
@section('content')
    <h1>Editar Diseño</h1>

    <div class="card">
        {{Form::open(['route' => ['design.update', $design->id], 'class' => 'form', 'method' => 'PUT'])}}

            @include('backend.designs.partials.inputs')
            <div class="col-lg-4">
                <select class="form-control" name="material_id">
                    @foreach($materials as $material)
                        <option value="{{$material->id}}">{{$material->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <input type="text"  class="form-control" name="quantity" placeholder="Cantidad">
            </div>
            <div class="col-lg-4">
                <input type="submit" name="add" value="Agregar" class="btn btn-primary">
            </div>
            @include('backend.designs.partials.tableDetails')
            <input type="submit" value="Guardar" class="btn btn-lg btn-success" name="update">

        {{Form::close()}}
    </div>

@endsection
@section('extra-js')
    <script type="text/javascript">
        /*$('[name="quit_form"]').one('submit', function(e){
            e.preventDefault();
            $.confirm({
                theme: 'supervan',
                title: 'Confirmar',
                content: 'Ejemplo',
                buttons: {
                    cancel: {
                        text: 'Cancelar',
                        action: function () {
                        }
                    },
                    confirm: {
                        text: 'Confirmar',
                        action: function () {
                        }
                    }
                }
            });
            //$(this).submit();
        });*/
    </script>
@endsection