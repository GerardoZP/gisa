@extends('generals.main')
@section('content')
    {{Form::open(['route'=> 'product.store','class'=> 'form','method'=>'POST'])}}
    @include('backend.products.partials.inputs')

    <div class="row">
        <div class="col-lg-4">
            Selecciona el diseño
            <select class="form-control" name="design_id" id="design_id">
                @foreach($designs  as $desing)
                    <option value="{{$desing->id}}">{{$desing->code}}</option>
                @endforeach
            </select>
        </div>
    </div>


    <br>
    <input type="submit" value="Guardar" class="btn btn-lg btn-success">
    {{Form::close()}}
@endsection