@extends('generals.main')
@section('content')
    <h1>Editar Producto</h1>

    <div class="card">
        {{Form::open(['route' => ['product.update', $product->id], 'class' => 'form', 'method' => 'PUT'])}}

        @include('backend.products.partials.inputs')
        <div class="row">
            <div class="col-lg-4">
                Selecciona el diseño
                <select class="form-control" name="design_id" id="design_id">
                    @foreach($designs  as $desing)
                            <option
                                    value="{{$desing->id}}"
                                    {{isset($product) ?
                                    $product->design_id == $desing->id ? 'selected' : '' :
                                    \Illuminate\Support\Facades\Input::old('desing_id') == $product->design_id ? 'selected':''}}
                            >{{$desing->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <br>
        <input type="submit" value="Guardar" class="btn btn-lg btn-success">
        {{Form::close()}}
    </div>
@endsection