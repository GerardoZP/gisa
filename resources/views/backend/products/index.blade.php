@extends('generals.main')
@section('search')
    {{Form::open(['route' => 'product.index', 'method' => 'get'])}}
    <input type="text" placeholder="Busqueda..."  name="filter">
    <input type="submit" value="">
    {{Form::close()}}
@endsection
@section('content')
    <h1>Productos</h1>
    <div class="row">
        <div class="col-lg-6">
            <br> <a href="{{route("product.create")}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br></a>
        </div>
    </div>
    @include('backend.products.partials.table')
@endsection