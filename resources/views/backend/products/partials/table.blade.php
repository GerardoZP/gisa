<div class="row">


        @foreach($designs as $design)
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Diseño : <strong>{{$design->code}}</strong></div>
                        <table class="table table-responsive">
                            <tr>
                                <th>ID</th>
                                <td>Tamaño</td>
                                <td>Precio</td>
                                <td>Inventario</td>
                                <td>Ver</td>
                            </tr>
                            @foreach($design->products as $aux)
                                    <tr>
                                        <td>{{$aux->id}}</td>
                                        <td>{{$aux->size}}</td>
                                        <td>{{$aux->price}}</td>
                                        <td>{{$aux->stock}}</td>
                                        <td>
                                            {{Form::open(['route' => ['product.edit', $aux->id], 'method' => 'get'])}}
                                            <input type="submit"  value="Editar" class="btn btn-info">
                                            {{Form::close()}}
                                        </td>
                                    </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            @endforeach

</div>