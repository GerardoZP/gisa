<div class="row">
    <div class="col-lg-4 form-group">
        <label>Número</label>
        <input type="text" name="size" placeholder="Punto del calzado" class="form-control" value="{{isset($product) ? $product->size : ''}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Inventario</label>
        <input type="text" name="stock" placeholder="Inventario" class="form-control" value="{{isset($product) ? $product->stock : ''}}">
    </div>

    <div class="col-lg-4 form-group">
        <label>Precio</label>
        <input type="text" name="price" placeholder="Precio" class="form-control" value="{{isset($product) ? $product->price : ''}}">
    </div>
</div>


