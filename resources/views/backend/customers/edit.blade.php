@extends('generals.main')
@section('content')
    <h1>Editar Cliente</h1>

    <div class="card">
        {{Form::open(['route' => ['customer.update', $customer->id], 'class' => 'form', 'method' => 'put'])}}

            @include('backend.customers.partials.inputs')
            <input type="submit" value="Guardar" class="btn btn-lg btn-success">

        {{Form::close()}}
    </div>

@endsection