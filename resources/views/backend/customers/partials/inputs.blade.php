<div class="row">
    <div class="col-lg-4 form-group">
        <label>Nombre</label>
        <input type="text" name="name" placeholder="Nombre" class="form-control" value="{{isset($customer) ? $customer->name : ''}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Primer apellido</label>
        <input type="text" name="firstname" placeholder="Primer apellido" class="form-control" value="{{isset($customer) ? $customer->firstname : ''}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Segundo apellido</label>
        <input type="text" name="lastname" placeholder="Segundo apellido" class="form-control" value="{{isset($customer) ? $customer->lastname : ''}}">
    </div>
</div>
<div class="row">
    <div class="col-lg-3 form-group">
        <label>Ciudad</label>
        <input type="text" name="city" placeholder="Ciudad" class="form-control" value="{{isset($customer) ? $customer->city : ''}}">
    </div>
    <div class="col-lg-3 form-group">
        <label>Zona</label>

        <select class="col-lg-3 form-control" name="zone_id" id="zone_id">
            @foreach($zones  as $zone)
                <option value="{{$zone->id}}"
                        {{isset($customer) ? $customer->zone_id == $zone->id ? 'selected' : '' : ''}}
                    >
                    {{$zone->id}} - {{$zone->name}}
                </option>
            @endforeach
        </select>
    </div>

    <div class="col-lg-3 form-group">
        <label>Correo electrónico</label>
        <input type="text" name="email" placeholder="Correo electrónico" class="form-control" value="{{isset($customer) ? $customer->email : ''}}">
    </div>
    <div class="col-lg-3 form-group">
        <label>RFC</label>
        <input type="text" name="rfc" placeholder="RFC" class="form-control" value="{{isset($customer) ? $customer->rfc : ''}}">
    </div>
</div>
