@extends('generals.main')
@section('search')
    {{Form::open(['route' => 'customer.index', 'method' => 'get'])}}
        <input type="text" placeholder="Busqueda..."  name="filter">
        <input type="submit" value="">
    {{Form::close()}}
@endsection
@section('content')
    <h1>Clientes</h1>

    <div class="col-lg-6">
        <br><a href="{{route('customer.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br>
    </div>

    @include('backend.customers.table')
@endsection

