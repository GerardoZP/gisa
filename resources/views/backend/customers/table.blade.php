<table class="table table-responsive">
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Ciudad</th>
        <th>Zona</th>
        <th>Email</th>
        <th>RFC</th>
        <th>Editar</th>
        <th>Borrar</th>
    </tr>
    @foreach($customers as $customer)
        <tr>
            <td>{{$customer->id}}</td>
            <td>{{$customer->name}} {{$customer->firstname}} {{$customer->lastname}}</td>
            <td>{{$customer->city}}</td>
            <td>{{$customer->zone->name}}</td>
            <td>{{$customer->email}}</td>
            <td>{{$customer->rfc}}</td>
            <td>
                {{Form::open(['route' => ['customer.edit', $customer->id], 'method' => 'get'])}}
                    <input type="submit"  value="Editar" class="btn btn-info">
                {{Form::close()}}
            </td>
            <td>
                {{Form::open(['route' => ['customer.destroy', $customer->id], 'method' => 'delete'])}}
                    <input type="submit" value="Eliminar" class="btn btn-danger" data-toggle="confirm" data-title="Confirmar" data-message="¿Estás seguro de eliminar este cliente?, También se eliminarán sus pedidos." data-type="danger">
                {{Form::close()}}
            </td>
        </tr>
    @endforeach
</table>


