@extends('generals.main')
@section('content')
    <h1>Ingresar Cliente</h1>

    <div class="card">
        {{Form::open(['route' => ['customer.store'], 'class' => 'form', 'method' => 'post'])}}

            @include('backend.customers.partials.inputs')
            <input type="submit" value="Guardar" class="btn btn-lg btn-success">

        {{Form::close()}}
    </div>

@endsection