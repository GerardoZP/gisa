@extends('generals.main')
@section('search')
    {{Form::open(['route' => 'provider.index', 'method' => 'get'])}}
    <input type="text" placeholder="Busqueda..."  name="filter">
    <input type="submit" value="">
    {{Form::close()}}
@endsection
@section('content')
    <h1>Proveedores</h1>
    <div class="col-lg-6">
       <br> <a href="{{route('provider.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br></a>
    </div>
    @include('backend.providers.partials.table')
@endsection