<table class="table table-responsive">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>RFC</th>
        <th>Teléfono</th>
        <th>Correo Electrónico</th>
        <th>Ver</th>
        <th></th>
    </tr>
    @foreach($providers as $provider)
        <tr>
            <td>{{$provider->id}}</td>
            <td>{{$provider->name}}</td>
            <td>{{$provider->rfc}}</td>
            <td>{{$provider->telephone}}</td>
            <td>{{$provider->email}}</td>
            <td>
                {{Form::open(['route' => ['provider.edit', $provider->id], 'method' => 'get'])}}
                    <input type="submit"  value="Editar" class="btn btn-info">
                {{Form::close()}}
            </td>
            <td>
                @if($provider->status == 1)
                    {{Form::open(['route' => ['provider.destroy', $provider->id ], 'method' => 'delete'])}}
                    <input type="hidden" value="desactivar" id="action" name="action" >
                    <input type="submit"  value="Desactivar" class="btn btn-danger">
                    {{Form::close()}}
                @else
                    {{Form::open(['route' => ['provider.destroy', $provider->id ], 'method' => 'delete'])}}
                        <input type="hidden" value="activar" id="action" name="action" >
                        <input type="submit"  value="Activar" class="btn btn-warning">
                    {{Form::close()}}
                @endif



            </td>
        </tr>
     @endforeach
</table>
