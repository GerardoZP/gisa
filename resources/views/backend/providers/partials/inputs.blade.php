<div class="row">
    <div class="col-lg-4 form-group">
        <label>Nombre</label>
        <input type="text" name="name" placeholder="Ingresa el Nombre" class="form-control" value="{{isset($provider) ? $provider->name : old('name') }}">
    </div>
    <div class="col-lg-4 form-group">
        <label>RFC</label>
        <input type="text" name="rfc" placeholder="Ingresa el RFC" class="form-control" value="{{isset($provider) ? $provider->rfc : old('rfc')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Telefono</label>
        <input type="text" name="telephone" placeholder="Ingresa el teléfono" class="form-control" value="{{isset($provider) ? $provider->telephone : old('telephone')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Correo Electronico</label>
        <input type="text" name="email" placeholder="Ingresa el correo electrónico" class="form-control" value="{{isset($provider) ? $provider->email : old('email')}}">
    </div>
</div>