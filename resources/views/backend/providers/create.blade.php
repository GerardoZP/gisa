@extends('generals.main')
@section('content')
    {{Form::open(['route'=> 'provider.store','class'=> 'form','method'=>'POST'])}}
        @include('backend.providers.partials.inputs')
        <input type="submit" value="Guardar" class="btn btn-lg btn-success">
    {{Form::close()}}
@endsection