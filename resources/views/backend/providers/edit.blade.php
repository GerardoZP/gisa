@extends('generals.main')
@section('content')
    <h1>Editar Proveedor</h1>

    <div class="card">
        {{Form::open(['route' => ['provider.update', $provider->id], 'class' => 'form', 'method' => 'PUT'])}}

            @include('backend.providers.partials.inputs')
            <input type="submit" value="Guardar" class="btn btn-lg btn-success">

        {{Form::close()}}
    </div>
    @endsection