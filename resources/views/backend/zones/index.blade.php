@extends('generals.main')
@section('content')
    <h1>Zonas</h1>

    <div class="col-lg-6">
        <br><a href="{{route('zone.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br>
    </div>
    @include('backend.zones.partials.table')
@endsection