<table class="table table-responsive">
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Editar</th>
    </tr>
    @foreach($zones as $zone)
        <tr>
            <td>{{$zone->id}}</td>
            <td>{{$zone->name}}</td>
            <td>
                {{Form::open(['route' => ['zone.edit', $zone->id], 'method' => 'get'])}}
                    <input type="submit" value="Editar" class="btn btn-info" >
                {{Form::close()}}
            </td>
        </tr>
    @endforeach
</table>