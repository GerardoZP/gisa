@extends('generals.main')
@section('content')
    <h1>Editar Zona</h1>
    {{Form::open(['route' => ['zone.update', $zone->id], 'method' => 'put'])}}
        @include('backend.zones.partials.inputs')
        <input type="submit" class="btn btn-success" value="Guardar">
    {{Form::close()}}
@endsection