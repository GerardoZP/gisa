@extends('generals.main')
@section('content')
    <h1>Ingresar Zona</h1>
    {{Form::open(['route' => 'zone.store', 'method' => 'post'])}}
        @include('backend.zones.partials.inputs')
        <input type="submit" class="btn btn-success" value="Guardar">
    {{Form::close()}}
@endsection