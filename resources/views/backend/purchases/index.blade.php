@extends('generals.main')
@section('content')
    <h1>Compras</h1>
    <div class="col-lg-6">
        <br><a href="{{route('purchase.create')}}"><button class="btn btn-lg btn-success">Nuevo</button></a><br><br>
    </div>
    @include('backend.purchases.partials.table')
@endsection