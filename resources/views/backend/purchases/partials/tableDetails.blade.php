<table class="table table-responsive">
    <tr>
        <th>Material</th>
        <th>Cantidad</th>
        <th>Subtotal</th>
        <th>Quitar</th>
    </tr>
    @for($i = 0; $i < sizeof($details); $i++)
        <tr>
            <td>{{$details[$i]->material->name}}</td>
            <td>{{$details[$i]->quantity}}</td>
            <td>{{$details[$i]->subtotal}}</td>

            <td>
                @if(Route::currentRouteName() == 'purchase.create')
                    {{Form::open(['route' =>  'purchase.store', 'method' => 'post'])}}
                    <input type="hidden" name="id" value="{{$i}}">
                    <input type="submit" class="btn btn-danger" name="quit" value="Quitar">
                    {{Form::close()}}

                @else
                    {{Form::open(['route' =>  ['purchase.update', $purchase->id], 'method' => 'put'])}}
                    <input type="hidden" name="id" value="{{$i}}">
                    <input type="hidden" name="purchase_id" value="{{$purchase->id}}">
                    <input type="hidden" name="quit" value="1">
                    <input type="submit" class="btn btn-danger" value="Quitar" >
                    {{Form::close()}}

                @endif
            </td>
        </tr>
    @endfor
</table>
