<div class="row">
    <div class="col-lg-4 form-group">
        <label>Fecha</label>
        <input type="date" name="purchaseDate" placeholder="Fecha" class="form-control" value="{{isset($purchase) ? $purchase->purchaseDate : old('purchaseDate')}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Proveedor</label>
        <select class="form-control" name="provider_id">
            @foreach($providers as $provider)
                <option
                        value="{{$provider->id}}"
                        {{isset($purchase) ?
                        $purchase->provider_id == $provider->id ? 'selected' : '' :
                         \Illuminate\Support\Facades\Input::old('provider_id') == $provider->id ? 'selected' : ''}}
                        >
                    {{$provider->name}}
                </option>
            @endforeach
        </select>
    </div>
</div>