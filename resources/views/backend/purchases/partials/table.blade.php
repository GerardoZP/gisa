<table class="table table-responsive">
    <tr>
        <th>Fecha de compra</th>
        <th>Estatus</th>
        <th>Total</th>
        <th>Proveedor</th>
        <th>Editar</th>
    </tr>
    @foreach($purchases as $purchase)
        <tr>
            <td>{{$purchase->purchaseDate}}</td>
            <td>{{$purchase->status}}</td>
            <td>{{$purchase->total}}</td>
            <td>{{$purchase->provider->name}}</td>
            <td>
                {{Form::open(['route' => ['purchase.edit', $purchase->id], 'method' => 'get'])}}
                    <input type="submit" value="Editar" class="btn btn-info">
                {{Form::close()}}
            </td>
        </tr>
    @endforeach
</table>
