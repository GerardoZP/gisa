@extends('generals.main')
@section('content')
    <h1>Editar Compra</h1>
    {{Form::open(['route' => ['purchase.update', $purchase->id], 'method' => 'put'])}}
    @include('backend.purchases.partials.inputs')

    <div class="row">
        <div class="col-lg-4">
            <select class="form-control" name="material_id">
                @foreach($materials as $material)
                    <option value="{{$material->id}}">{{$material->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <input type="text"  class="form-control" name="quantity" placeholder="Cantidad">
        </div>
        <div class="col-lg-4">
            <input type="submit" name="add" value="Agregar" class="btn btn-primary">
        </div>
        @include('backend.purchases.partials.tableDetails')

        <div class="row">
            <div class="col-lg-4">
                <input type="text"  class="form-control" name="total" placeholder="Total" readonly value="{{$total}}" }}>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <input type="submit" name="update" value="Guardar" class="btn btn-success">
        </div>
    </div>
    <br/>

    {{Form::close()}}

@endsection