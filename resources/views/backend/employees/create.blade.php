@extends('generals.main')
@section('content')
    <h1>Ingresar Empleado</h1>

    <div class="card">
        {{Form::open(['route' => ['employee.store'], 'class' => 'form', 'method' => 'post'])}}

        @include('backend.employees.partials.inputs')
        <input type="submit" value="Guardar" class="btn btn-lg btn-success">

        {{Form::close()}}
    </div>
    @endsection