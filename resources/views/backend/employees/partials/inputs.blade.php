<div class="row">
    <div class="col-lg-4 form-group">
        <label>Nombre</label>
        <input type="text" name="name" placeholder="Nombre" class="form-control" value="{{isset($employees) ? $employees->name : ''}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Primer apellido</label>
        <input type="text" name="firstname" placeholder="Primer apellido" class="form-control" value="{{isset($employees) ? $employees->firstname : ''}}">
    </div>
    <div class="col-lg-4 form-group">
        <label>Segundo apellido</label>
        <input type="text" name="lastname" placeholder="Segundo apellido" class="form-control" value="{{isset($employees) ? $employees->lastname : ''}}">
    </div>
</div>
