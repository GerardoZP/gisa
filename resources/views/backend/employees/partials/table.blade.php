<table class="table table-responsive">
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>A. Paterno</th>
        <th>A. Materno</th>
        <th></th>
        <th></th>
    </tr>
    @foreach($employees as $employee)
        <tr>
            <td>{{$employee->id}}</td>
            <td>{{$employee->name}}</td>
            <td>{{$employee->firstname}}</td>
            <td>{{$employee->lastname}}</td>
            <td>
                {{Form::open(['route' => ['employee.edit', $employee->id], 'method' => 'get'])}}
                <input type="submit"  value="Editar" class="btn btn-info">
                {{Form::close()}}
            </td>
            <td>
                {{Form::open(['route' => ['employee.destroy', $employee->id], 'method' => 'delete'])}}
                <input type="submit" value="Eliminar" class="btn btn-danger" data-toggle="confirm" data-title="Confirmar" data-message="¿Estás seguro de eliminar este empleado?." data-type="danger">
                {{Form::close()}}
            </td>
        </tr>
        @endforeach
</table>