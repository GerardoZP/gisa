@extends('generals.main')
@section('content')
    <h1>Editar Empleado</h1>
    {{Form::open(['route' => ['employee.update', $employees->id], 'class' => 'form', 'method' => 'put'])}}

    @include('backend.employees.partials.inputs')
    <input type="submit" value="Guardar" class="btn btn-lg btn-success">

    {{Form::close()}}
    @endsection