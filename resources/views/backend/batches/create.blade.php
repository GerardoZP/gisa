@extends('generals.main')
@section('content')
    <h1>Nuevo lote</h1>
    {{Form::open(['route' => 'batch.store', 'method' => 'post'])}}
        @include('backend.batches.partials.inputs')
        @include('backend.batches.partials.batchTable')
        <input type="submit" name="new" value="Guardar" class="btn btn-success">
    {{Form::close()}}
@endsection