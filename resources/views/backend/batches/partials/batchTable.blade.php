<table class="table table-responsive">
    <tr>

        <th>Producto</th>
        <th>Diseño</th>
        <th>Talla</th>
        <th>Cantidad a programar</th>
    </tr>
    @foreach(session('batchDetails') as $detail)
        <tr>
            <td>{{$detail->product->id}}</td>
            <td>{{$detail->product->design->code}}</td>
            <td>{{$detail->product->size}}</td>
            <td>{{$detail->quantity}}</td>
        </tr>
    @endforeach
</table>