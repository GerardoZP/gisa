<div class="price-block-main">
    <div class="price-tables">
        @foreach($batches as $batch)
            @php
                $capacity = 0;
                $forProduction = 0;
                $complete = true;

            @endphp
            @foreach($toProduction as $t)
                @if($t['id'] == $batch['design_id'])
                    @php $capacity = $t['capacity'] @endphp
                @endif
            @endforeach
            @foreach($details as $d)
                @if($batch->design_id == $d->design_id)
                    @if($d->stock < $d->total)
                        @php
                            $complete = false;
                            $forProduction += ($d->total - $d->stock);
                        @endphp
                    @endif
                @endif
            @endforeach
            {{Form::open(['route' => 'batch.store', 'method' => 'post', 'id' => 'form-program'])}}
                <input type="hidden" name="design_id" value="{{$batch->design_id}}"/>
                <div class="col-md-4 price-grid">
                    <div class="price-block" >
                        <div class="
                        @if($complete)
                                {{"price-gd-top pric-clr"}}
                        @elseif($forProduction <= $capacity)
                            {{"price-gd-top pric-clr1"}}
                        @else
                            {{"price-gd-top pric-clr3"}}
                        @endif
                        "
                        >
                            <h4>{{$batch['code']}}</h4>
                            <h3>{{$batch['total']}}</h3>
                            <h5>Fecha más cercana de entrega: {{$batch['date']}}</h5>
                        </div>
                        <div class="price-gd-bottom">
                            <div class="price-list">
                                <ul>
                                    <li>En inventario:
                                        @foreach($stocks as $stock)
                                            @if($stock->design_id == $batch->design_id)
                                                {{$stock->stock}}
                                            @endif
                                        @endforeach
                                    </li>
                                    <li>Material para fabricar:
                                        {{$capacity}}
                                    </li>
                                    Diseño Talla: Pedido/Inventario

                                    @foreach($details as $d)
                                        @if($batch->design_id == $d->design_id)
                                            <li>{{$d->code . ' ' . $d->size}}: {{$d->total}}/{{$d->stock}}</li>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        @if($complete)
                            <!--<div class="price-selet pric-clr" >
                                <a class="popup-with-zoom-anim" name="linkDelivers" href="#small-dialog">Programar entregas</a>
                            </div>-->
                        @elseif($forProduction <= $capacity)
                            <div class="price-selet pric-clr1" >
                                <a class="popup-with-zoom-anim" name="linkBatchs" href="#small-dialog">Programar lote</a>
                            </div>
                        @else
                            <div class="price-selet pric-clr3" >
                                <a class="popup-with-zoom-anim" name="linkPurchases" href="#small-dialog">Programar compra</a>
                            </div>
                        @endif


                    </div>
                </div>
            {{Form::close()}}
        @endforeach
    </div>
</div>
<div class="clearfix"></div>