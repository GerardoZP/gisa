<table class="table">
    <h1>DEtails</h1>
    <tr>
        <th>product_id</th>
        <th>size</th>
        <th>code</th>
        <th>design_id</th>
        <th>total</th>
        <th>date</th>
        <th>stock</th>
    </tr>
    @foreach($details as $detail)
        <tr>
            <td>{{$detail->product_id}}</td>
            <td>{{$detail->size}}</td>
            <td>{{$detail->code}}</td>
            <td>{{$detail->design_id}}</td>
            <td>{{$detail->total}}</td>
            <td>{{$detail->date}}</td>
            <td>{{$detail->stock}}</td>
        </tr>
        @endforeach

</table>

<table class="table">
    <h1>BAtchs</h1>
    <tr>
        <td>design_id</td>
        <td>code</td>
        <td>stock</td>
        <td>total</td>
        <td>date</td>
    </tr>
    @foreach($batches as $batch)
        <tr>
            <td>{{$batch['design_id']}}</td>
            <td>{{$batch['code']}}</td>
            <td>{{$batch['stock']}}</td>
            <td>{{$batch['total']}}</td>
            <td>{{$batch['date']}}</td>
        </tr>
    @endforeach
</table>