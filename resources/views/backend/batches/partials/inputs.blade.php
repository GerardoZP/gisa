<div class="row">
    <div class="col-lg-4 form-group">
        <label>Fecha</label>
        <input type="date" name="startDate" placeholder="Fecha" class="form-control" value="{{isset($batch) ? $batch->startDate : $date}}" >
    </div>
    <div class="col-lg-4 form-group">
        <label>Diseños</label>
        <select class="form-control" name="design_id" readonly>
                <option value="{{isset($batch) ? $batch->design_id : $design->id}}">
                    {{isset($batch) ? $batch->design->code : $design->code}}
                </option>
        </select>
    </div>
</div>