<table class="table table-responsive">
    <tr>
        <th>Fecha de inicio</th>
        <th>Diseño</th>
        <th>Progreso</th>
        <th>Editar</th>
    </tr>
    @foreach($actives as $active)
        @if($active->progress != 100)
            <tr>
                <td>{{$active->startDate}}</td>
                <td>{{$active->design->code}}</td>
                <td>

                    <div class="progress progress-striped active">
                        <div class="bar yellow" style="width:{{$active->progress}}%;"></div>
                    </div></td>
                <td>
                    {{Form::open(['route' => ['batch.edit', $active->id], 'method' => 'get'])}}
                        <input type="submit" class="btn btn-info" value="Editar">
                    {{Form::close()}}

                </td>
            </tr>
        @endif
    @endforeach
</table>