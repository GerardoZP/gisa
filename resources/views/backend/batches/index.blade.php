@extends('generals.main')
@section('content')
    <h1>Programación de lotes</h1>
    <!--@include('backend.batches.partials.test')-->

    @include('backend.batches.partials.activesTable')
    @include('backend.batches.partials.table')

@endsection
@section('extra-js')
    <script type="text/javascript">
        $(document).ready(function(){
            var linksDelivers = document.getElementsByName('linkDelivers');
            var linksBatchs = document.getElementsByName('linkBatchs');
            var linksPurchases = document.getElementsByName('linkPurchases');
            for(i = 0; i < linksDelivers.length; i++){
                linksDelivers[i].addEventListener('click', function(){
                    form = $(this).closest('form');
                    form.append('<input type="hidden" name="deliver" value="1"/>');
                    form.submit();
                });
            }
            for(i = 0; i < linksBatchs.length; i++){
                linksBatchs[i].addEventListener('click', function(){
                    form = $(this).closest('form');
                    form.append('<input type="hidden" name="batch" value="1"/>');
                    form.submit();
                });
            }
            for(i = 0; i < linksPurchases.length; i++){
                linksPurchases[i].addEventListener('click', function(){
                    form = $(this).closest('form');
                    form.append('<input type="hidden" name="purchase" value="1"/>');
                    form.submit();
                });
            }
        });
    </script>
@endsection