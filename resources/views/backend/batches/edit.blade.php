@extends('generals.main')
@section('content')
    <h1>Actualizar progreso de lote</h1>
    @include('backend.batches.partials.inputs')
    <table class="table">
        <tr>

            <th>Producto</th>
            <th>Diseño</th>
            <th>Talla</th>
            <th>Cantidad a programar</th>
            <th>Estado</th>
            <th>Cambiar</th>
        </tr>
        @foreach($batch->details as $detail)
            <tr>
                <td>{{$detail->product->id}}</td>
                <td>{{$detail->product->design->code}}</td>
                <td>{{$detail->product->size}}</td>
                <td>{{$detail->quantity}}</td>
                <td>
                    @if($detail->status == 0)
                        <span class="label label-danger">Programado</span>
                    @elseif($detail->status == 15)
                        <span class="label label-warning">En corte</span>
                    @elseif($detail->status == 25)
                        <span class="label label-warning">En preliminar</span>
                    @elseif($detail->status == 40)
                        <span class="label label-warning">En pespunte</span>
                    @elseif($detail->status == 45)
                        <span class="label label-info">En gancho</span>
                    @elseif($detail->status == 60)
                        <span class="label label-info">En montado</span>
                    @elseif($detail->status == 80)
                        <span class="label label-info">En adorno</span>
                    @elseif($detail->status == 100)
                        <span class="label label-success">Terminado</span>
                    @endif
                </td>
                <td>
                    {{Form::open(['route' => ['batch.update', $batch->id], 'method' => 'put'])}}
                        <input type="hidden" name="id" value="{{$detail->id}}">
                        @if($detail->status == 0)
                            <input type="submit" class="btn btn-info" name="1" value="Cambiar a corte"/>
                        @elseif($detail->status == 15)
                            <input type="submit" class="btn btn-info" name="2" value="Cambiar a preliminar"/>
                        @elseif($detail->status == 25)
                            <input type="submit" class="btn btn-info" name="3" value="Cambiar a pespunte"/>
                        @elseif($detail->status == 40)
                            <input type="submit" class="btn btn-info" name="4" value="Cambiar a gancho"/>
                        @elseif($detail->status == 45)
                            <input type="submit" class="btn btn-info" name="5" value="Cambiar a montado"/>
                        @elseif($detail->status == 60)
                            <input type="submit" class="btn btn-info" name="6" value="Cambiar a adorno"/>
                        @elseif($detail->status == 80)
                            <input type="submit" class="btn btn-info" name="7" value="Cambiar a terminado"/>
                        @endif
                    {{Form::close()}}
                </td>
            </tr>
        @endforeach
    </table>

@endsection