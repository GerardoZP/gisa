
<html>
<head>
    @include('generals.head')
</head>
<body>
<div class="login-page">
    <div class="login-main">
        <div class="login-head">
            <h1>Login</h1>
            <img src="{{asset('img/logo.png')}}" width="25%" style="display: block;
      margin-left: auto;
      margin-right: auto;
      border:none;">
        </div>
        <div class="login-block">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{Form::open(['route' => 'login', 'method' => 'post'])}}
                <input type="text" name="user" placeholder="Usuario" required="">
                <input type="password" name="password" class="lock" placeholder="Contraseña">
                <div class="forgot-top-grids">
                    <!--<div class="forgot-grid">
                        <ul>
                            <li>
                                <input type="checkbox" id="brand1" value="">
                                <label for="brand1"><span></span>Remember me</label>
                            </li>
                        </ul>
                    </div>--->
                    <div class="clearfix"> </div>
                </div>
                <input type="submit" name="Sign In" value="Iniciar sesión">
            {{Form::close()}}

        </div>
    </div>
</div>

<!--scrolling js-->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<!-- mother grid end here-->
</body>
</html>




