<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);


Route::group(['middleware' => 'auth'], function (){

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index')->name('home');

    Route::group(['namespace' => 'Customers'], function(){
        Route::pattern('customer', '[0-9]+');
        Route::resource('customer', 'CustomersController');
    });
    Route::group(['namespace' => 'Providers'], function(){
        Route::pattern('provider', '[0-9]+');
        Route::resource('provider', 'ProvidersController');
    });
    Route::group(['namespace' => 'Designs'], function(){
        Route::pattern('design', '[0-9]+');
        Route::resource('design', 'DesignsController');
    });
    Route::group(['namespace' => 'Products'], function(){
        Route::pattern('product', '[0-9]+');
        Route::resource('product', 'ProductsController');
    });
    Route::group(['namespace' => 'Materials'], function(){
        Route::pattern('material', '[0-9]+');
        Route::resource('material', 'MaterialsController');
    });

    Route::group(['namespace' => 'Purchases'], function(){
        Route::pattern('purchase', '[0-9]+');
        Route::resource('purchase', 'PurchasesController');
    });
    Route::group(['namespace' => 'Orders'], function(){
        Route::pattern('order', '[0-9]+');
        Route::resource('order', 'OrdersController');
    });

    Route::group(['namespace' => 'Zones'], function(){
        Route::pattern('zone', '[0-9]+');
        Route::resource('zone', 'ZonesController');
    });

    Route::group(['namespace' => 'Employees'], function(){
        Route::pattern('employee', '[0-9]+');
        Route::resource('employee', 'EmployeesController');
    });

    Route::get('/reports', ['as' => 'reports', 'uses' => 'Reports\ReportsController@index']);
    Route::group(['namespace' => 'Batchs'], function(){
        Route::pattern('batch', '[0-9]+');
        Route::resource('batch','BatchsController');
    });

});
